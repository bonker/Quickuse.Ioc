﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Quickuse.Ioc;
using Quickuse.Service;
using Quickuse.WebApp.Models;

namespace Quickuse.WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public IStudentService StudentService { get; set; }
        public IStudentService StudentService2 { get; set; }

        public IStudentService _studentService;
        public IStudentService _studentService2;

        public HomeController(ILogger<HomeController> logger, IStudentService studentService, IStudentService studentService2)
        {
            _studentService = studentService;
            _studentService2 = studentService2;
            _logger = logger;
        }

        public IActionResult Index()
        {
            //var student = ServiceLocater.GetService<IStudentService>();
            _studentService.SayHello("quickuse");
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
