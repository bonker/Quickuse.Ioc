﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quickuse.Service
{
    public class InterceptorService
    {
        public static Func<Type[]> Types => () =>
        {
            List<Type> typeList = new List<Type>();

            typeList.Add(typeof(LogInterceptor));

            return typeList.ToArray();
        };
    }
}
