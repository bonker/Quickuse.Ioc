﻿using Quickuse.Ioc;
using System;
using System.Collections.Generic;
using System.Text;

namespace Quickuse.Service
{
    //[NamedAttribute("Student")]
    public class StudentService : IStudentService
    {

        /// <summary>
        /// SayHello
        /// </summary>
        /// <param name="studentName"></param>
        /// <returns></returns>
        public virtual string SayHello(string studentName)
        {
            return $"Student{studentName}说大家好：{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}";
        }
    }
}
