﻿using Quickuse.Ioc;
using System;
using System.Collections.Generic;
using System.Text;

namespace Quickuse.Service
{
    [PropertiesAutowiredAttribute]
    [InstancePerRequestAttribute]
    public interface IStudentService : IDependency
    {
        /// <summary>
        /// SayHello
        /// </summary>
        /// <param name="studentName"></param>
        /// <returns></returns>
        string SayHello(string studentName);
    }

    public interface IBaseService : IDependency
    {

    }
}
