﻿using Autofac;
using Quickuse.Ioc;
using Quickuse.Service;
using System;
using System.Collections.Generic;

namespace Quickuse.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //ServiceLocater.RegisterSingleService<Stu, IStu>();
            ServiceLocater.RegisterSingleService<Student>(new Student());
            //ServiceLocater.OnRegister += ServiceLocater_OnRegister;
            ServiceLocater.Init(typeof(LogInterceptor));
            var student = ServiceLocater.GetService<Student>();

            student.SpeakLocal();
            //var stu = ServiceLocater.GetService<IStu>();
            //stu.Show();

            //var studentservice = ServiceLocater.GetService<IStudentService>();
            //Console.WriteLine(studentservice.SayHello("闲僧"));
            Console.WriteLine($"ok");
            Console.ReadKey();
        }

        private static void ServiceLocater_OnRegister(ContainerBuilder builder)
        {
            builder.RegisterInstance<IStu>(new Stu()).SingleInstance();
            builder.RegisterType<Student>();
        }

        public static Func<Type[]> InterceptorTypes => () =>
        {
            List<Type> typeList = new List<Type>()
            {

            };

            return typeList.ToArray();
        };
    }
}
