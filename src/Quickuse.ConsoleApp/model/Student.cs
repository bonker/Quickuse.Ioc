﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Quickuse.ConsoleApp
{
    public class Student
    {

        #region LocalLock
        public virtual string SpeakLocal()
        {
            var threadId = Thread.CurrentThread.ManagedThreadId.ToString();

            Console.WriteLine($"{threadId}本地默认锁");
            Thread.Sleep(1000);
            return "本地默认锁";
        }

        public virtual string SpeakLocal1()
        {
            var threadId = Thread.CurrentThread.ManagedThreadId.ToString();
            Console.WriteLine($"{threadId}本地指定key锁");
            Thread.Sleep(5000);
            return $"本地指定key锁";
        }

        public virtual string SpeakLocal2()
        {
            var threadId = Thread.CurrentThread.ManagedThreadId.ToString();
            Console.WriteLine($"{threadId}本地指定key锁");
            Thread.Sleep(2000);
            return "本地指定key锁";
        }

        /// <summary>
        /// 方法参数key
        /// </summary>
        /// <returns></returns>
        public virtual string SpeakLocal3(string arg1, string arg2)
        {
            var threadId = Thread.CurrentThread.ManagedThreadId.ToString();
            Console.WriteLine($"{threadId}本地参数key锁");
            Thread.Sleep(1000);
            return "本地参数key锁";
        }
        #endregion

        #region GlobalLock
        /// <summary>
        /// 全局默认锁
        /// </summary>
        /// <returns></returns>
        public virtual string SpeakGlobal()
        {
            var threadId = Thread.CurrentThread.ManagedThreadId.ToString();
            Console.WriteLine($"{threadId}全局默认锁");
            Thread.Sleep(1000);
            return "全局默认锁";
        }

        /// <summary>
        /// 全局默认锁
        /// </summary>
        /// <returns></returns>
        public virtual string SpeakGloba2(string arg1, string arg2)
        {
            var threadId = Thread.CurrentThread.ManagedThreadId.ToString();
            Console.WriteLine($"{threadId}全局默认锁");
            Thread.Sleep(1000);
            return "全局默认锁";
        }
        #endregion
    }
}
