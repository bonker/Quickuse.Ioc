﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quickuse.ConsoleApp
{
    public interface IStu
    {
        void Show();
    }

    public class Stu : IStu
    {
        public void Show()
        {
            Console.WriteLine("我是测试");
        }
    }
}
