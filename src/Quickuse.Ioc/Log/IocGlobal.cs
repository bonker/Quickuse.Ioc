﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quickuse.Ioc
{
    /// <summary>
    /// 
    /// </summary>
    public class IocGlobal
    {
        /// <summary>
        /// 
        /// </summary>
        public static LogManager Log = new LogManager();

        /// <summary>
        /// 调试环境
        /// </summary>
        public static bool IsDeBug
        {
            get
            {
#if DEBUG
                return true;
#endif
                //获取环境变量是否为调试环境
                bool.TryParse(Environment.GetEnvironmentVariable("ISDEBUG"), out bool isdebug);
                return isdebug;
            }
        }
    }
}
