﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quickuse.Ioc
{
    /// <summary>
    /// 跟踪日志
    /// </summary>
    /// <param name="message"></param>
    public delegate void TraceLogEventHandler(string message);

    /// <summary>
    /// 信息日志
    /// </summary>
    /// <param name="message"></param>
    public delegate void InfoLogEventHandler(string message);

    /// <summary>
    /// 警告日志
    /// </summary>
    /// <param name="message"></param>
    public delegate void WarnLogEventHandler(string message);

    /// <summary>
    /// 错误日志
    /// </summary>
    /// <param name="message"></param>
    public delegate void ErrorLogEventHandler(string message);

    /// <summary>
    /// 调试调试
    /// </summary>
    /// <param name="message"></param>
    public delegate void DebugLogEventHandler(string message);

    /// <summary>
    /// 故障日志
    /// </summary>
    /// <param name="message"></param>
    public delegate void CriticalLogEventHandler(string message);
}
