﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace Quickuse.Ioc
{
    /// <summary>
    /// 日志管理
    /// </summary>
    public class LogManager
    {

        /// <summary>
        /// 跟踪日志
        /// </summary>
        [method: CompilerGenerated]
        [CompilerGenerated]
        public event TraceLogEventHandler OnTrace;

        /// <summary>
        /// 调试日志
        /// </summary>
        [method: CompilerGenerated]
        [CompilerGenerated]
        public event DebugLogEventHandler OnDebug;

        /// <summary>
        /// 信息日志
        /// </summary>
        [method: CompilerGenerated]
        [CompilerGenerated]
        public event InfoLogEventHandler OnInfo;

        /// <summary>
        /// 警告日志
        /// </summary>
        [method: CompilerGenerated]
        [CompilerGenerated]
        public event WarnLogEventHandler OnWarn;

        /// <summary>
        /// 异常日志
        /// </summary>
        [method: CompilerGenerated]
        [CompilerGenerated]
        public event ErrorLogEventHandler OnError;

        /// <summary>
        /// 故障日志
        /// </summary>
        [method: CompilerGenerated]
        [CompilerGenerated]
        public event CriticalLogEventHandler OnCritical;

        /// <summary>
        /// 跟踪日志
        /// </summary>
        /// <param name="message"></param>
        internal void Trace(string message)
        {
            if (this.OnTrace != null)
            {
                this.OnTrace(message);
            }
            else
            {
                Log("trace", message);
            }
        }

        /// <summary>
        /// 调试日志
        /// </summary>
        /// <param name="message"></param>
        internal void Debug(string message)
        {
            if (IocGlobal.IsDeBug)
            {
                if (this.OnDebug != null)
                {
                    this.OnDebug(message);
                }
                else
                {
                    Log("debug", message);
                }
            }
        }

        /// <summary>
        /// 信息日志
        /// </summary>
        /// <param name="message"></param>
        internal void Info(string message)
        {
            if (this.OnInfo != null)
            {
                this.OnInfo(message);
            }
            else
            {
                Log("info", message);
            }
        }

        /// <summary>
        /// 警告日志
        /// </summary>
        /// <param name="message"></param>
        internal void Warn(string message)
        {
            if (this.OnWarn != null)
            {
                this.OnWarn(message);
            }
            else
            {
                Log("warn", message);
            }
        }

        /// <summary>
        /// 异常日志
        /// </summary>
        /// <param name="message"></param>
        internal void Error(string message)
        {
            if (this.OnError != null)
            {
                this.OnError(message);
            }
            else
            {
                Log("error", message);
            }
        }

        /// <summary>
        /// 故障日志
        /// </summary>
        /// <param name="message"></param>
        internal void Critical(string message)
        {
            if (this.OnCritical != null)
            {
                this.OnCritical(message);
            }
            else
            {
                Log("critical", message);
            }
        }

        /// <summary>
        /// 日志
        /// </summary>
        /// <param name="level">级别</param>
        /// <param name="message">信息</param>
        internal void Log(string level, string message)
        {
            Console.WriteLine($"[[{level}]]{message}[[{DateTime.Now:yyyy-MM-dd HH:mm:ss fff}]]");
        }
    }
}
