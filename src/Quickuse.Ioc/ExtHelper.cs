﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quickuse.Ioc
{
    internal static class ExtHelper
    {

        /// <summary>
        /// ToArrayValue
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        internal static string[] ToArrayValue(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return new string[] { };
            }

            return value.Split(new[] { ";", "；", ",", "，" }, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
