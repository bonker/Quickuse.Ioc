﻿using Autofac;
using Autofac.Extras.DynamicProxy;
using System;

namespace Quickuse.Ioc
{
    /// <summary>
    /// Service Locater
    /// </summary>
    public class ServiceLocater
    {

        /// <summary>
        /// Init ServiceLocater
        /// </summary>
        /// <param name="interceptorType">拦截器类型</param>
        public static void Init(params Type[] interceptorType)
        {
            ContainerBuilder builder = new ContainerBuilder();

            TypeFinder typeFinder = new TypeFinder();
            foreach (var item in typeFinder.Assemblies)
            {
                DependencyConfig.RegisterDependency(item, builder, interceptorType);
            }

            OnRegister?.Invoke(builder);

            DependencyResolver.SetResolver(builder.Build());
        }

        /// <summary>
        /// Init ServiceLocater
        /// </summary>
        /// <param name="interceptorTypeFunc">Interceptor Type Func</param>
        public static void Init(Func<Type[]> interceptorTypeFunc)
        {
            var types = interceptorTypeFunc?.Invoke() ?? new Type[] { };
            Init(types);
        }

        /// <summary>
        /// Get Service 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetService<T>()
        {
            return DependencyResolver.Resolve<T>();
        }

        /// <summary>
        /// Get Service 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="named">别名</param>
        /// <returns></returns>
        public static T GetService<T>(string named)
        {
            return DependencyResolver.Resolve<T>(named);
        }

        /// <summary>
        /// OnRegister
        /// </summary>
        public static event RegisterEventHandler OnRegister;

        #region 注册服务
        /// <summary>
        /// 注册单例服务
        /// </summary>
        /// <typeparam name="TService">类型</typeparam>
        /// <param name="service">实例</param>
        public static void RegisterSingleService<TService>(TService service)
        {
            DependencyConfig.RegisterSingleService(typeof(TService), service);
        }

        /// <summary>
        /// 注册单例服务
        /// </summary>
        /// <typeparam name="TService"></typeparam>
        /// <typeparam name="ITService"></typeparam>
        public static void RegisterSingleService<TService, ITService>()
        {
            DependencyConfig.RegisterSingleService(typeof(TService), typeof(ITService));
        }

        /// <summary>
        /// 注册作用域服务
        /// </summary>
        /// <typeparam name="TService"></typeparam>
        /// <param name="service"></param>
        
        public static void RegisterScopedService<TService>(TService service)
        {
            DependencyConfig.RegisterScopedService(typeof(TService), service);
        }

        /// <summary>
        /// 注册作用域服务
        /// </summary>
        /// <typeparam name="TService"></typeparam>
        /// <typeparam name="ITService"></typeparam>
        public static void RegisterScopedService<TService, ITService>()
        {
            DependencyConfig.RegisterScopedService(typeof(TService), typeof(ITService));
        }

        /// <summary>
        /// 注册瞬时服务
        /// </summary>
        /// <typeparam name="TService"></typeparam>
        /// <param name="service"></param>
        public static void RegisterPerDependencyService<TService>(TService service)
        {
            DependencyConfig.RegisterPerDependencyService(typeof(TService), service);
        }

        /// <summary>
        /// 注册瞬时服务
        /// </summary>
        /// <typeparam name="TService"></typeparam>
        /// <typeparam name="ITService"></typeparam>
        public static void RegisterPerDependencyService<TService, ITService>()
        {
            DependencyConfig.RegisterPerDependencyService(typeof(TService), typeof(ITService));
        }
        #endregion
    }
}
