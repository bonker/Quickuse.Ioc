﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quickuse.Ioc
{
    /// <summary>
    /// 别名
    /// </summary>
    public class NamedAttribute : Attribute
    {
        /// <summary>
        /// 别名
        /// </summary>
        public string Named { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_named">别名</param>
        public NamedAttribute(string _named)
        {
            Named = _named;
        }
    }
}
