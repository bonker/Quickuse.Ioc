﻿using System;

namespace Quickuse.Ioc
{
    /// <summary>
    /// 每个请求一个实例
    /// </summary>
    public class InstancePerRequestAttribute : Attribute
    {

    }
}
