﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quickuse.Ioc
{
    /// <summary>
    /// 注入类型
    /// </summary>
    internal enum RegisterTypeEnum
    {
        /// <summary>
        /// 单例
        /// </summary>
        Single = 1,

        /// <summary>
        /// 作用域（）
        /// </summary>
        PerLifetimeScope = 2,

        /// <summary>
        /// 瞬时
        /// </summary>
        PerDependency = 3,
    }
}
