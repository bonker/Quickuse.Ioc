﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace Quickuse.Ioc
{
    /// <summary>
    /// 快搜应用Autofac
    /// </summary>
    public class QuickuseAutofac
    {
        ///// <summary>
        ///// AutofacServiceProviderFactory
        ///// </summary>
        ///// <returns></returns>
        //public static AutofacServiceProviderFactory ServiceProvider1 => new AutofacServiceProviderFactory(config => config.AddQuickuseDependency());

        /// <summary>
        /// AutofacServiceProviderFactory
        /// </summary>
        /// <returns></returns>
        public static Func<HostBuilderContext, IServiceProviderFactory<ContainerBuilder>> ServiceProvider =>
            (context) => new AutofacServiceProviderFactory(config => config.AddQuickuseDependency(context));

        /// <summary>
        /// Use Service Provider
        /// </summary>
        /// <param name="context"></param>
        /// <param name="interceptorTypes">拦截器类型</param>
        /// <returns></returns>
        public static IServiceProviderFactory<ContainerBuilder> UseServiceProvider(HostBuilderContext context, params Type[] interceptorTypes)
        {
            return new AutofacServiceProviderFactory(config => config.AddQuickuseDependency(context, interceptorTypes));
        }

        /// <summary>
        /// Use Service Provider
        /// </summary>
        /// <param name="context"></param>
        /// <param name="interceptorTypeFunc">Interceptor Type Func</param>
        /// <returns></returns>
        public static IServiceProviderFactory<ContainerBuilder> UseServiceProvider(HostBuilderContext context, Func<Type[]> interceptorTypeFunc)
        {
            var types = interceptorTypeFunc?.Invoke() ?? new Type[] { };
            return new AutofacServiceProviderFactory(config => config.AddQuickuseDependency(context, types));
        }
    }
}
