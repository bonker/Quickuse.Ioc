﻿using Autofac;
using Autofac.Extras.DynamicProxy;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Reflection;

namespace Quickuse.Ioc
{
    /// <summary>
    /// 依赖扩展
    /// </summary>
    public static class DependencyBuilderExtensions
    {

        /// <summary>
        /// 添加依赖
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="context"></param>
        /// <param name="interceptorTypes"></param>
        public static void AddQuickuseDependency(this ContainerBuilder builder, HostBuilderContext context, params Type[] interceptorTypes)
        {
            TypeFinder typeFinder = new TypeFinder(context.Configuration);
            foreach (Assembly assembly in typeFinder.Assemblies)
            {
                DependencyConfig.RegisterDependency(assembly, builder, interceptorTypes);
            }

            builder.RegisterBuildCallback(DependencyResolver.SetResolver);
        }

        /// <summary>
        /// 添加 QuickuseIoc
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddQuickuseIoc(this IServiceCollection services)
        {
            return services;
        }

        /// <summary>
        /// 添加 QuickuseIoc
        /// </summary>
        /// <param name="hostBuilder">The Microsoft.Extensions.Hosting.IHostBuilder to configure.</param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static IContainer AddQuickuseIoc(this IServiceCollection hostBuilder, Func<IServiceCollection, IContainer> action)
        {
            if (action == null)
            {
                throw new ArgumentNullException(nameof(action));
            }
            return action(hostBuilder);
        }

        /// <summary>
        /// 使用 Quickuse.Ioc 
        /// </summary>
        /// <param name="hostBuilder">The Microsoft.Extensions.Hosting.IHostBuilder to configure.</param>
        /// <returns>The same instance of the Microsoft.Extensions.Hosting.IHostBuilder for chaining.</returns>
        public static IHostBuilder UseQuickuseIoc(this IHostBuilder hostBuilder)
        {
            return hostBuilder.UseServiceProviderFactory(QuickuseAutofac.ServiceProvider);
        }

        /// <summary>
        /// 使用 Quickuse.Ioc 
        /// </summary>
        /// <param name="hostBuilder">The Microsoft.Extensions.Hosting.IHostBuilder to configure.</param>
        /// <param name="interceptorTypes">拦截器类型</param>
        /// <returns>The same instance of the Microsoft.Extensions.Hosting.IHostBuilder for chaining.</returns>
        public static IHostBuilder UseQuickuseIoc(this IHostBuilder hostBuilder, params Type[] interceptorTypes)
        {
            return hostBuilder.UseServiceProviderFactory(context =>
            {
                return QuickuseAutofac.UseServiceProvider(context, interceptorTypes);
            });
        }
    }
}
