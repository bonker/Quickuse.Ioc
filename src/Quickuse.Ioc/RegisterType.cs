﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quickuse.Ioc
{
    /// <summary>
    /// 注册类型
    /// </summary>
    internal class RegisterType
    {
        /// <summary>
        /// 类型（单例、作用域、瞬时）
        /// </summary>
        public RegisterTypeEnum RegisterEnum { get; set; }

        /// <summary>
        /// 注册方式(0类型，1实例)
        /// </summary>
        public int RegisterMode { get; set; }

        /// <summary>
        /// 接口类型
        /// </summary>
        public Type IAssemblyType { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public Type AssemblyType { get; set; }

        /// <summary>
        /// 实例
        /// </summary>
        public object Instance { get; set; }
    }
}
