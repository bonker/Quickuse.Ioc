# Quickuse.Ioc

#### 介绍

**Quickuse.Ioc**快速应用依赖注入组件，提供便捷使用 **Autofac** 类库方式的组件，同时利用 **Castle.Core** 实现动态代理，完成AOP切面编程，可以根据项目需求定制代理器，使开发人员在编写业务逻辑时可以专心于核心业务，而不用过多的关注于其他业务逻辑的实现，这不但提高了开发效率，而且增强了代码的可维护性。比如可以结合 **[Quikcuse.Lock](http://quickuse.gitee.io/quickuse.lock)** 组件实现标记特性的方式实现本地锁、分布式锁。



######  支持标注特性来声明实例的生命周期,具体使用方式，请参考在线文档，并欢迎指正. 


在线文档： http://quickuse.gitee.io/quickuse.ioc



`netstandard2.1`

