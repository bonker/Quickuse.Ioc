## 配置依赖

* 设置可以注入的接口`IDependency`

例如：

``` C#
public interface IStudentService : IDependency
{
    /// <summary>
    /// SayHello
    /// </summary>
    /// <param name="studentName"></param>
    /// <returns></returns>
    string SayHello(string studentName);
}
```

* 使用基类设置可以注入的接口

例如：

``` C#
public interface IStudentService : IBaseService
{
    /// <summary>
    /// SayHello
    /// </summary>
    /// <param name="studentName"></param>
    /// <returns></returns>
    string SayHello(string studentName);
}

public interface IBaseService : IDependency
{

}
```

`注意基类必须包含IBase字符串`



## 实例生命周期

通过给接口类标注特性来选择实例的生命周期

### 单一实例  

使用单一实例作用域, 在根容器和所有嵌套作用域内所有的请求都将会返回同一个实例.

例如：`SingleInstanceAttribute`

``` C#
    [SingleInstanceAttribute]
    public interface IStudentService : IDependency
    {
        /// <summary>
        /// SayHello
        /// </summary>
        /// <param name="studentName"></param>
        /// <returns></returns>
        string SayHello(string studentName);
    }
```

> 

### 每个请求一个实例   

一些应用本身就适合 "request" 类型的语法, 例如 ASP.NET web forms 和 MVC 应用. 在这些应用类型中, 拥有一组 "每个请求一个" 的单例非常有用."

每个请求一个实例建立于每个匹配生命周期一个实例之上 , 通过另外提供了一个众所周知的作用域标签, 一个方便注册的方法, 和对一些普通应用类型的集成. 而在这些背后, 其实它本质上还是一个每个匹配生命周期一个实例.

这就意味着如果你试图解析一个注册为每个请求一个实例的组件但是并没有当前请求... 你将会得到一个异常.

例如：`InstancePerRequestAttribute`

``` C#
    [InstancePerRequestAttribute]
    public interface IStudentService : IDependency
    {
        /// <summary>
        /// SayHello
        /// </summary>
        /// <param name="studentName"></param>
        /// <returns></returns>
        string SayHello(string studentName);
    }
```

### 每个依赖一个实例 

在其他容器中也被称为 'transient' 或者 'factory' . 使用每个依赖的作用域, 对于一个服务每次请求都会返回一个唯一的实例.

如果没有指定特定的选项, 它将是默认的.

例如：`不用标注特性`

``` C#
    public interface IStudentService : IDependency
    {
        /// <summary>
        /// SayHello
        /// </summary>
        /// <param name="studentName"></param>
        /// <returns></returns>
        string SayHello(string studentName);
    }
```



***

## 实例别名

通过给实现类标注特性设置服务别名

例如：`NamedAttribute`

``` C#
    [NamedAttribute("Student")]
    public class StudentService : IStudentService
    {
        /// <summary>
        /// SayHello
        /// </summary>
        /// <param name="studentName"></param>
        /// <returns></returns>
        public string SayHello(string studentName)
        {
            return $"{studentName}说大家好：{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}";
        }
    }
```



## 设置允许属性注入

通过给接口类标注特性设置允许属性注入

例如：`PropertiesAutowiredAttribute`

``` C#
    [PropertiesAutowiredAttribute]
    public interface IStudentService : IDependency
    {
        /// <summary>
        /// SayHello
        /// </summary>
        /// <param name="studentName"></param>
        /// <returns></returns>
        string SayHello(string studentName);
    }
```

`注意：可以使用属性注入的实例必须是容器创建的实例并且设置了可以属性注入，非容器创建的实例或没有设置允许属性注入，都无法实现属性注入....这点很重要`

### Asp.Net Core 设置属性注入

需要再`Startup`文件中添加控制器服务，并设置控制器允许被属性注入

* 添加控制器服务 `AddControllersAsServices()`

  ![image-20200416001709023](..\lib\img\image-20200416001709023.png)

  

* 设置控制器运行被属性注入

  ![image-20200416001836785](..\lib\img\image-20200416001836785.png)

``` C#
        public void ConfigureContainer(ContainerBuilder containerBuilder)
        {
            //获取所有控制器类型并使用属性注入
            var controllerBaseType = typeof(ControllerBase);

            containerBuilder.RegisterAssemblyTypes(typeof(Program).Assembly)
                .Where(t => controllerBaseType.IsAssignableFrom(t) && t != controllerBaseType)
                .PropertiesAutowired();
        }
```

