# 开始配置

## Asp.net Core 使用方式

###  修改默认容器

>  替换默认容器

``` C#
.UseServiceProviderFactory(QuickuseAutofac.ServiceProvider)
```

![image-20200415225147815](..\lib\img\image-20200415225147815.png)

> 替换默认容器并设置拦截类型，快速注入代理类

``` C#
.UseServiceProviderFactory(context => QuickuseAutofac.UseServiceProvider(context, typeof(LogInterceptor)))
```

+ 代理服务 `引用Autofac.Extras.DynamicProxy包`

  ``` C#
  public class LogInterceptor : IInterceptor
  {
      public void Intercept(IInvocation invocation)
      {
          Console.WriteLine("方法执行前");
          invocation.Proceed();
          Console.WriteLine("方法执行后");
      }
  }
  ```

  


### 配置文件规则

   * 搜索指定类型类库注入`建议配置`

    {
    	"ioc:assemblies": "Quickuse",
    }

   `配置说明：可以不配置，默认搜索全部，可以使用分隔符[,;]配置多个,一般配置当前项目名、或程序集名称`

   

   * 排除指定类型

    {
    "unioc:assemblies": "Common"
    }

   ​	`配置说明：可以不配置，默认不排除。如果配置了，就会优先排除指定程序集，可以使用分隔符[,;]配置多个，一般配置注入出现异常的程序集名称`

   

***

## .Net Core 使用方式

### 初始化容器服务

``` C#
ServiceLocater.Init();
```

![image-20200415225323780](..\lib\img\image-20200415225323780.png)

或者

``` C#
ServiceLocater.Init(typeof(LogInterceptor));
```

初始化的同时设置代理类。



###### 比如某一个服务需要切面写入一些逻辑处理，可以新建一个代理类，并将需要处理的方法设置为虚方法`virtual` 就可以实现切面处理

