<p align="center">
	-- QQ： <a href="http://wpa.qq.com/msgrd?v=3&uin=929630279&site=qq&menu=yes" target="_blank">929263027</a> --
</p>

***

#### 介绍

 **Quickuse.Ioc**快速应用依赖注入组件，提供便捷使用 **Autofac** 类库方式的组件，同时利用 **Castle.Core** 实现动态代理，完成AOP切面编程，可以根据项目需求定制代理器，使开发人员在编写业务逻辑时可以专心于核心业务，而不用过多的关注于其他业务逻辑的实现，这不但提高了开发效率，而且增强了代码的可维护性。比如可以结合 **[Quikcuse.Lock](http://quickuse.gitee.io/quickuse.lock)** 组件实现标记特性的方式实现本地锁、分布式锁。

#### 安装教程
 * 安装方式1
> 打开程序包管理器控制台输入 `Install-Package Quickuse.Ioc` 

* 安装方式2  
> 打开管理Nuget程序包 搜索`Quickuse.Ioc`  安装

